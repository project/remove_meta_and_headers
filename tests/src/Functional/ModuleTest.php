<?php

namespace Drupal\Tests\remove_meta_and_headers\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group remove_meta_and_headers
 */
class ModuleTest extends BrowserTestBase {

  /**
   * Set Default theme for Non-Markup test.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testCheckMetaTagRemoved() {

  }

}
