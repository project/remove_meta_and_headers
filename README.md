## INTRODUCTION


The Remove META and Headers module helps to fix some security scan related
Issue on drupal project. Using this module you can remove "X-Generator"
Item from the response header and other header items (Server, PHP Version).
Also, hide meta tag for Generator.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/remove_meta_and_headers

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/remove_meta_and_headers

## INSTALLATION


 * Install as you would normally install a contributed Drupal module.
   Visit:  https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

## CONFIGURATION

 * For configuration settings:
   `/admin/config/remove_meta_and_headers/settings`

## MAINTAINERS


**Current maintainers:**

 * Ashwin Parmar (ashwinparmar_accenture) - https://www.drupal.org/user/3169583
 
 * Nilesh Chhatbar (Nilesh Chhatbar) - https://www.drupal.org/user/2788025
